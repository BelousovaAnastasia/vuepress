module.exports = {
    title: 'GitLab ❤️ VuePress', 
    description: '!Проверка! Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'
}